Source: reel
Section: ruby
Priority: optional
Maintainer: Debian Ruby Extras Maintainers <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Hleb Valoshka <375gnu@gmail.com>
Build-Depends: debhelper (>= 9~),
               gem2deb,
               rake,
               ruby-celluloid (>= 0.15.1),
               ruby-celluloid-io (>= 0.15.0),
               ruby-certificate-authority,
               ruby-http (>= 0.6.0.pre),
               ruby-http-parser.rb (>= 0.6.0),
               ruby-rspec,
               ruby-websocket-driver (>= 0.5.1),
               ruby-websocket-parser (>= 0.1.6)
Standards-Version: 3.9.8
Vcs-Git: https://anonscm.debian.org/git/pkg-ruby-extras/reel.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-ruby-extras/reel.git
Homepage: https://github.com/celluloid/reel
Testsuite: autopkgtest-pkg-ruby
XS-Ruby-Versions: all

Package: reel
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ruby | ruby-interpreter,
         ruby-celluloid (>= 0.15.1),
         ruby-celluloid-io (>= 0.15.0),
         ruby-http (>= 0.6.0~pre),
         ruby-http-parser.rb (>= 0.6.0),
         ruby-websocket-driver (>= 0.5.1),
         ${misc:Depends},
         ${shlibs:Depends}
Description: Celluloid::IO-powered HTTP server
 Reel is a fast, non-blocking "evented" web server built on http_parser.rb,
 websocket_parser, Celluloid::IO, and nio4r. Thanks to Celluloid, Reel also
 works great for multithreaded applications and provides traditional
 multithreaded blocking I/O support too.
 .
 Connections to Reel can be either non-blocking and handled entirely within the
 Reel::Server thread (handling HTTP, HTTPS, or UNIX sockets), or the same
 connections can be dispatched to worker threads where they will perform
 ordinary blocking IO. Reel provides no built-in thread pool, however you can
 build one yourself using Celluloid.pool, or because Celluloid already pools
 threads to begin with, you can simply use an actor per connection.
